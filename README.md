This repo is a copy of Angular's Karma examples, migrated migrated to Jest + Spectator.

A working Karma example repo can be found [here](https://github.com/muratkeremozcan/books/tree/master/Angular_with_Typescript/angular-unit-testing-with-Karma).

Clone, cd in, `npm i && npm run test`.

<details><summary>Migrating from Karma to Jest</summary>

[Why use Jest](https://slides.com/msz_technology/deck)?

You can do it [manually](https://dev.to/alfredoperez/angular-10-setting-up-jest-2m0l), or automatically with [Angular Jest Schematic from Briebug](https://github.com/briebug/jest-schematic)

To get started:

```bash
npm install jest @types/jest jest-preset-angular --save-dev

npm uninstall karma karma-chrome-launcher karma-coverage-istanbul-reporter karma-jasmine karma-jasmine-html-reporter @types/jasmine @types/jasminewd2 jasmine-core jasmine-spec-reporter

ng add @briebug/jest-schematic
```

The schematic will do these:

```bash
DELETE karma.conf.js
DELETE src/test.ts
CREATE jest.config.js (180 bytes)
CREATE setup-jest.ts (860 bytes)
CREATE test-config.helper.ts (611 bytes)
UPDATE package.json (1322 bytes)
UPDATE angular.json (3592 bytes)
UPDATE tsconfig.spec.json (330 bytes)
```

Instead of `jest.config.js`, move the settings to package.json. I like to add to package.json the settings in the [manual instructions](https://dev.to/alfredoperez/angular-10-setting-up-jest-2m0l). Enhance this as you need it. Here is what I have in `package.json`:

```json
  "jest": {
    "preset": "jest-preset-angular",
    "setupFilesAfterEnv": [
      "<rootDir>/setup-jest.ts"
    ],
    "testPathIgnorePatterns": [
      "<rootDir>/node_modules/",
      "<rootDir>/dist/"
    ],
    "globals": {
      "ts-jest": {
        "tsconfig": "<rootDir>/tsconfig.spec.json",
        "stringifyContentPathRegex": "\\.html$"
      }
    },
    "moduleNameMapper": {
      "@core/(.*)": "<rootDir>/src/app/core/$1"
    }
  }
```

I also like to replace default test script in `package.json` and add some new ones:

```json
"scripts": {
  ...
  "test": "jest",
  "test:coverage": "jest --collectCoverage",
  "test:watch": "jest --watch",
}
```

In `setup-jest.js`, change the first line from `import 'jest-preset-angular';` to `import 'jest-preset-angular/setup-jest`. This will get rid of the Jest warning when running tests. In a future version of briebug schematic, this may be taken care of.

Spying and mocking is different in Jest. You will have to change these manually.

If using Spectator, `npm i -D @ngneat/spectator`. In the spec files change `import from '@ngneat/spectator'` to `import from '@ngneat/spectator/jest'`.

<details>

This is the base state of the application.

</details>
</details>

<br></br>

<details><summary>PWA migration</summary>

## PWA

A [Service Worker](https://angular.io/guide/service-worker-intro) is a script that runs in the web browser and manages caching for an application. Using a service worker to reduce dependency on the network can significantly improve the user experience.

<br> </br>

### Add the service worker to the project

`ng add @angular/pwa --project angular-unit-testing`

<br> </br>

### Verify the changes

- `ngsw-config.json` should get created. This file indicates glob patterns for what gets cached, and is configurable.

  <details><summary>There are 2 important properties here: </summary>

  1. `installMode` determines how the resources are initially cached, that is, when the user first visits the application and the service worker is registered for the first time.

  2. `updateMode` works for resources already in the cache.

  These properties can have 2 values– `prefetch` and `lazy`.

  `prefetch` means that the service worker will go ahead and download all resources in the group as soon as possible and put them into the cache.
  This uses more data initially but ensures that resources are already in the cache, even when the application goes offline later.

  `lazy` means that the service worker will only download the resources when they are requested.

  </details>

- `angular.json` build section gets updated.

  If you want to enable service workers in deployments, double check that it is also copied to other config sections (dev, int, preview etc.).

  ```json
    "serviceWorker": true,
    "ngswConfigPath": "ngsw-config.json"
  ```

- `app-module.ts` gets updated:

  ```typescript
  ServiceWorkerModule.register("ngsw-worker.js", {
    enabled: environment.production,
    // Register the ServiceWorker as soon as the app is stable
    // or after 30 seconds (whichever comes first).
    registrationStrategy: "registerWhenStable:30000",
  });
  ```

<br>

### Test that it works

Build in prod mode and locally test utilizing [`http-server`](https://www.npmjs.com/package/http-server) package.

> Service workers are only available in Prod mode.

**Arrange:**

```bash
ng build --prod

npm i -g http-server

http-server -p 8080 -c-1 dist/angular-unit-testing    ## -c-1 disables caching
```

Nav to `http://127.0.0.1:8080` , use incognito.

**Act:**

Using Devtools > Network tab, turn the network off and refresh the app.

**Assert:**

The app should work as normal and the browser should not show disconnected page `There is no Internet connection`.

Devtools > Network tab > Size column should show value `(Service Worker)` for the network resources.

**Additional test**
Devtools > Application tab > and choose Service Workers on the left. You should see that the service worker is enabled.

</details>

<br></br>

<details><summary>Setup Cypress</summary>

### [Migrate from Protractor to Cypress](https://blog.briebug.com/blog/switching-to-cypress-from-protractor-in-less-than-30-seconds)

This will replace Protractor with Cypress and update your dependencies and project files.

```bash
npm install -g @briebug/cypress-schematic
ng add @briebug/cypress-schematic
```

You can optionally leave the changes it makes to `angular.json`, and `package.json` they do not do harm.
Personally I do not utilize them. So I remove the "e2e", "cypress-run" and "cypress-open" properties from `angular.json`. I also remove the `briebug/cypress-schematic` package from `package.json`.

```json
  "e2e": { ...
  },
  "cypress-run": { ...
  },
  "cypress-open": { ...
  }
```

### Core recommended settings

- Use \*`index.js` instead of `index.ts` under `cypress/support`, because it works better with Cypress plugins that may not support TypeScript.

- Recommended settings for `cypress.json`.

  ```json
  {
    "baseUrl": "http://localhost:4200",
    "videoUploadOnPasses": false, // will be cost effective in CI
    "retries": {
      "runMode": 2, // retries in CI, or locally running with cypress:run
      "openMode": 0
    },
    "chromeWebSecurity": false, // will help with x-origin
    "$schema": "https://on.cypress.io/cypress.schema.json" // will safeguard against misconfiguration of cypress.json
  }
  ```

- Use config files

  A good pattern for testing different deployments (development, staging, production etc.) is using config files.

  I like to use `@bahmutov/cypress-extends` to have the custom config files I create under `cypress/config` folder inherit from the base `cypress.json` file. This is not yet included in the base Cypress install. Refer to `plugins/index.js` `cypress/config/` folder to sample the setup.

  ```json
  //  cypress/config/dev.json
  {
    "extends": "../../cypress.json",
    "baseUrl": "https://your-deployed-app.com"
  }
  ```

- Add 2 scripts to package.json, to open Cypress with test runner and to run Cypress headed. The `--config-file cypress/config/local.json` is optional, but needed to utilize config files.
- ```json
  "cypress:open": "cypress open --config-file cypress/config/local.json",
  "cypress:run": "cypress run --config-file cypress/config/local.json"
  ```

### Start Cypress

Serve your app with `npm run start` and on another tab start Cypress with `npm run cypress:open`.

To execute the tests in CI or without the test runner UI locally, use `npm run cypress:run`.

</details>

<br></br>

<details><summary>Setup CI</summary>

## CI

- Make Cypress an optional dependency instead of a dev dependency. If for any reason CI fails to install Cypress, it does not matter, because we will be using the Cypress included docker image in e2e stage. This approach will also speed up the build stage by a factor.

  ```json
  "optionalDependencies": {
    "cypress": "7.3.0"
  },
  ```

- `npm install --save-dev start-server-and-test` . [start-server-and-test](https://www.npmjs.com/package/start-server-and-test) makes it easy to spin a localhost in CI and run e2e against it.

  Locally try out the script `yarn easy` to see it serve localhost and then open cypress.

  In CI we use a version of it:

  ```yml
  # spins up a local UI server, waits for it to start, executes Cypress tests against localhost, stops the server
  script:
    - >
      yarn server-test start http://localhost:4200
      'cypress run --record --parallel --browser chrome --group local --tag 'branch' --config-file cypress/config/local.json'
  ```

- GitLab provides a few optimizations: Caching, Acyclic patterns, Parallelization, Resource groups. These are all applicable to Cypress CI setup as well. Have a look at the yml files from master for details.

- Parallelization: when you open Cypress runner, you default to the Tests tab. Check out the Runs tab. This is where you begin with [Cypress Dashboard](https://www.cypress.io/dashboard/). It has 500 test executions for free monthly, and they are willing to give unlimited free trial if you ask for it, so do not worry.

  - On the upper right use Login to login the dashboard https://dashboard.cypress.io/login . I use GitHub.
  - Connect to Dashboard and create a project.
  - From here on, Cypress docs are excellent. But, effectively all you need is to set the projectId in `cypress.json` and/or the config files (`"projectId": "4mhoqq"`) and use the record key.
  - Test a recording locally `yarn cypress run --record --key 29b708ae-6839-4446-8d68-d93ad6ca81f9`
  - [As advised in the docs](https://docs.cypress.io/guides/guides/command-line#cypress-run) set the key as an environment variable in CI (already done in CI, but not in your local environment, obviously). If you set this env var locally, you can omit the key parameter: `yarn cypress run --record `
  - You can view all the runs at the [dashboard](https://dashboard.cypress.io/projects/4mhoqq/runs?branches=%5B%5D&committers=%5B%5D&flaky=%5B%5D&page=1&status=%5B%5D&tags=%5B%5D&timeRange=%7B%22startDate%22%3A%221970-01-01%22%2C%22endDate%22%3A%222038-01-19%22%7D) since this is a public project.

</details>

<br></br>

<details><summary> Setup Combined Coverage</summary>

Follow the [blog post](https://dev.to/muratkeremozcan/combined-unit-e2e-code-coverage-case-study-on-a-real-life-system-using-angular-jest-cypress-gitlab-35nk) for a detailed walk-through of combined code coverage setup.

</details>

<br></br>
